from db import get_job_data
from services import Services
import re
import json
from db import insert_job_type
from db import get_jobs


services = Services()


def cleaner(test_string):
    """
    This function is used to clean the job title string.
    """
    test_string = test_string.replace("\n", ", ")
    test_string = test_string.replace("-", " ")
    #remove numerical values from string
    test_string = re.sub(r'\d+', '', test_string)
    #remove . from string
    test_string = test_string.replace(".", "")
    test_string = test_string.replace("  ", "")

    final_string = list(set(test_string.split(",")))
    
    # check if "" is in the list
    if ' ' in final_string:
        final_string.remove(' ')
    
    if '' in final_string:
        final_string.remove('')
    
    return final_string


def clean_text(text):
    
    """
        text: a string
        return: modified initial string
    """

    REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
    BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')

    text = str(text)
    text = text.lower() # lowercase text
    text = REPLACE_BY_SPACE_RE.sub(' ', text) # replace REPLACE_BY_SPACE_RE symbols by space in text
    text = BAD_SYMBOLS_RE.sub('', text) # delete symbols which are in BAD_SYMBOLS_RE from text
    return text


def job_details_cleaner(test_string):
    """
    This function is used to clean the job title string.
    """
    test_string = test_string.replace("\n", ", ")
    test_string = test_string.replace("-", " ")
    #remove numerical values from string
    # test_string = re.sub(r'\d+', '', test_string)
    #remove . from string
    test_string = test_string.replace(".", "")
    test_string = test_string.replace("  ", "")

    final_string = list(set(test_string.split(",")))
    
    # check if "" is in the list
    if ' ' in final_string:
        final_string.remove(' ')
    
    if '' in final_string:
        final_string.remove('')
    
    return final_string


def job_type_parser(desc_list):
    types = []
    for i in desc_list:
        desc = i
        text = desc.replace("/n","")
        
        if "full time" in text:
            # 1 represents 'full time'
            types.append(1)
            
        elif "part time" in text:
            # 2 represents 'part time'
            types.append(2)
            
        elif "both" in text:
            types.append(1)
            types.append(2)
            
    return types


def extract_salary(s):
    """
    This function is used to extract salary form a given
    job description
    """
    
    if "$" in s:
        return s[s.index('$'):]
    else:
        return s


def job_type_prediction(job_description):
    """
    This function is used to extract type of a job form a given 
    job description
    """

    job_type = services.job_type_prediction(job_description)
    job_type = cleaner(job_type)
    job_type = job_type_parser(job_type)

    return job_type


def job_roles_prediction(job_description):
    """
    This function is used to extract roles of a job form a given 
    job description
    """

    job_roles = services.job_roles_prediction(job_description)
    job_roles = cleaner(job_roles)

    return job_roles


def job_salary_prediction(job_description):
    """
    This function is used to extract salary of a job form a given 
    job description
    """
    clean_job_description = clean_text(job_description)
    
    print(clean_job_description)
    
    job_salary = services.job_salary_prediction(clean_job_description)
    job_salary = extract_salary(job_salary)

    return job_salary


def job_benefit_requirement_qualification(job_description):
    
    # create a empty json object
    result_dict = {}
        
    benefit_and_all = services.benefit_req_qualifi_prediction(job_description)
        
    try:
        benefit_and_all = benefit_and_all.split('A. ', 1)[1]
    except:
        pass

    try:
        qualification = benefit_and_all.split("Qualifications:",1)[1]
        result_dict["qualification"] = job_details_cleaner(qualification)

    except:
        pass

    try:
        benefits_and_requirenments = benefit_and_all.split("Qualifications:",1)[0]
        requirements = benefits_and_requirenments.split("Requirements:",1)[1]
        result_dict["requirements"] = job_details_cleaner(requirements)

    except:
        pass

    try:
        benefits = benefits_and_requirenments.split("Requirements:",1)[0]
        benefits = benefits.split("Benefits:",1)[1]
        result_dict["benefits"] = job_details_cleaner(benefits) 
                    
    except:
        pass
    
    return result_dict
