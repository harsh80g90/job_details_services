#connect mysql to python
import os

# def get_PSQLconnection():
    
#     connection = psycopg2.connect(
#                                 host=os.getenv("DBHOST"),
#                                 user=os.getenv("DBUSER"),
#                                 password=os.getenv("DBPASS"),
#                                 dbname=os.getenv("DBNAME"),
#                                 port=os.getenv("DBPORT")
#                             )

#     return connection

def get_job_data():
    """
    This function is used to get the job roles from the database.
    """
    db = get_PSQLconnection()
    cur = db.cursor()
    
    query = "select job.id, organization.name, html_job_description from jobs.job join jobs.organization ON organization_id = jobs.organization.ids.job join jobs.organization ON organization_id = jobs.organization.id limit 2"

    cur.execute(query)
    job_list = cur.fetchall()

    cur.close()
    db.close()
    
    return job_list

def get_org_list():
    
    #get organization list from given path
    organization_file = open(os.getenv("organization_path"), "r")
    test_list = organization_file.readlines()
    organization_list = [x.strip() for x in test_list]
    
    organization_file.close()
    return organization_list

def get_jobs():
    
    db = get_PSQLconnection()
    cur = db.cursor()

    organization_list = get_org_list()

    data = []

    for organization in organization_list:
        query = "select job.id, html_job_description, job_title, organization.name from jobs.job join jobs.organization ON organization_id = jobs.organization.id where organization.name like '"+ organization +"%' limit 10"
        cur.execute(query)
        job_list = cur.fetchall()

        for row in job_list:
            data.append(row)

    unique_org =tuple(organization_list)
    query = "select job.id, html_job_description, job_title ,organization.name from jobs.job join jobs.organization ON organization_id = jobs.organization.id where organization.name NOT IN {} limit 2". format(str(unique_org))
    cur.execute(query)
    job_list = cur.fetchall()
    for row in job_list:
        data.append(row)

    cur.close()
    db.close()

    return data

def insert_job_type(job_id, job_type_id):
    
    db = get_PSQLconnection()
    cur = db.cursor()
    
    #insert job type
    query = "INSERT INTO jobs.job2job_type (job_id, job_type_id) VALUES (%s, %s)"
    cur.execute(query, (job_id, job_type_id))
    print("insert record at job_id:", job_id)
    db.commit()
    cur.close()
    db.close()