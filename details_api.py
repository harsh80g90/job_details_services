import os
from client import job_benefit_requirement_qualification

from flask import Flask,request,jsonify
from flask import app

from flask import Flask
from flask_cors import CORS


app=Flask(__name__)

CORS(app)

@app.route('/req_qual_benefit',methods= ["POST", "GET"])

def sim_question():
    
    description = request.form.get('description')
    # description.headers['Access-Control-Allow-Origin'] = '*'
    result = job_benefit_requirement_qualification(description)
    return jsonify(result)
    
if __name__=="__main__":
    app.run(host=os.getenv("IP"), port=5030)
    # app.run()
